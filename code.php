<?php
function getLetterGrade($gradeRange){
	if($gradeRange >= 98 && $gradeRange <= 100){
		return "$gradeRange is equivalent to A+";
	}else if($gradeRange >= 95 && $gradeRange <= 97){	
		return "$gradeRange is equivalent to A";	
	}else if($gradeRange >= 92 && $gradeRange <= 94){	
		return "$gradeRange is equivalent to A-";	
	}else if($gradeRange >= 89 && $gradeRange <= 91){	
		return "$gradeRange is equivalent to B+";	
	}else if($gradeRange >= 86 && $gradeRange <= 88){	
		return "$gradeRange is equivalent to B";	
	}else if($gradeRange >= 83 && $gradeRange <= 85){	
		return "$gradeRange is equivalent to B-";	
	}else if($gradeRange >= 80 && $gradeRange <= 82){	
		return "$gradeRange is equivalent to C+";	
	}else if($gradeRange >= 77 && $gradeRange <= 79){	
		return "$gradeRange is equivalent to C";	
	}else if($gradeRange >= 75 && $gradeRange <= 76){	
		return "$gradeRange is equivalent to C-";	
	}else if($gradeRange < 75){	
		return "$gradeRange is equivalent to F";	
	}else{
		return "Grade is out of range";
	}
	
}